#!/bin/bash

. /scripts/common.sh

_start_service(){
    _log "Démarrage du serveur MySQL...\n"

    count_service=$(pgrep mysql | wc -l);
    if [ $count_service -gt 0 ]; then
        _log "Serveur MySQL déjà démarré!\n"
    fi

    service mysql start > /dev/null 2>&1
    _nonzero_fatale $? 'Impossible de démarrer le serveur MySQL!'
}

_sql_uniq(){
    mysql -u root -p"$ROOT_PASSWORD" -e "$1" > /dev/null 2>&1
    return $?
}

_sql_multiple_transaction(){
    _sql_uniq 'START TRANSACTION;'

    for var in "$@"; do
        _sql_uniq "$var"
        if [ "$?" != '0' ]; then
            _sql_uniq 'ROLLBACK;'
            return 1
        fi
    done

    _sql_uniq 'COMMIT;'
    return 0
}

# >>>>>>>>>> Mot de passe root généré aléatoirement

declare -g ROOT_PASSWORD
ROOT_PASSWORD=`_echo_gen_rand`

# >>>>>>>>>> Checks

# Initialisation des variables globales basées sur la présence de certaines
# variables d'environnement. En cas de problème, le script est stoppé.

declare -g USER_DATABASE
USER_DATABASE=`echo "$MARIADB_USER_DATABASE" | xargs`
_empty_fatale "$USER_DATABASE" 'MARIADB_USER_DATABASE non défini!'

declare -g USER_NAME
USER_NAME=`echo "$MARIADB_USER_NAME" | xargs`
_empty_fatale "$USER_NAME" 'MARIADB_USER_NAME non défini!'

declare -g USER_PASSWORD
USER_PASSWORD=`echo "$MARIADB_USER_PASSWORD" | xargs`
_empty_fatale "$USER_PASSWORD" 'MARIADB_USER_PASSWORD non défini!'

declare -g DATABASE_ALREADY_EXISTS
if [ -d /var/lib/mysql/mysql ]; then
    DATABASE_ALREADY_EXISTS='true';
fi

# >>>>>>>>>> Démarrage du service (import si nécessaire)

# S'il n'y a pas de base de données, on lance une initialisation
# complète. Sinon, on lance le service.

if [ -z "$DATABASE_ALREADY_EXISTS" ]; then
    # La commande "mysql_install_db" permet de réinitialiser l'ensemble
    # des ressources minimales nécessaires au fonctionnement du service.

    mysql_install_db > /dev/null 2>&1
    _nonzero_fatale $? "Impossible d'initialiser la base de données!"

    # Démarrage du service.

    _start_service

    # Création de l'utilisateur "root".

    mysqladmin -u root password "$ROOT_PASSWORD"
    _nonzero_fatale $? "Impossible de créer l'utilisateur root!"

    # Suppression des éléments inutiles ou de tests.
    # > Requêtes non-critiques!

    _sql_uniq "DELETE FROM mysql.user WHERE User='';"
    _sql_uniq "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.3.0.1', '::1');"
    _sql_uniq "DROP DATABASE IF EXISTS test;"
    _sql_uniq "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"

    # Création de la base de données utilisateur.

    _log "Création de l'utilisateur '$USER_NAME' et de sa base de données associée '$USER_DATABASE'...\n"

    _sql_multiple_transaction "CREATE DATABASE $USER_DATABASE;" \
        "CREATE USER '$USER_NAME'@'localhost' IDENTIFIED BY '$USER_PASSWORD';" \
        "GRANT ALL ON $USER_DATABASE.* TO '$USER_NAME'@'localhost';" \
        "FLUSH PRIVILEGES;"

    _nonzero_fatale $? "La création de la base de données a échoué!"

    # Import de la base de données utilisateur, si fichier "install.sql" existant!

    if [ -f /app/init/mariadb/install.sql ]; then
        _log "Import de la base de données '/app/init/mariadb/install.sql'...\n"

        mysql -u "$USER_NAME" -p"$USER_PASSWORD" $USER_DATABASE \
            < /app/init/mariadb/install.sql > /dev/null 2>&1

        _nonzero_fatale $? "L'import a échoué!"
    fi
else
    _start_service
fi

# Suppression des éléments inutiles.
rm -rf /app/init/mariadb

exit 0