#!/bin/bash

_exec_exit_on_error() {
    $1
    EXIT_CODE=$?
    if [ "$EXIT_CODE" != '0' ]; then
        exit $EXIT_CODE
    fi
}

_log(){
    printf "$@"
}

_warning(){
    _log "$@\n" >&2
}

_error(){
    _warning "$@"
    exit 0
}

_fatale(){
    _warning "$@"
    exit 1
}

_nonzero_warning(){
    if [ "$1" != '0' ]; then
        _warning "$2"
    fi
}

_nonzero_error(){
    if [ "$1" != '0' ]; then
        _error "$2"
    fi
}

_nonzero_fatale(){
    if [ "$1" != '0' ]; then
        _fatale "$2"
    fi
}

_empty_fatale(){
    value=`echo "$1" | xargs`
    if [ "$value" = '' ]; then
        _fatale "$2";
    fi
}

_echo_ts(){
    # Pour vérifier l'existence de la Timezone ciblée, on vérifie la présence
    # du fichier dans le répertoire zoneinfo. Sinon on place sur "Europe/Paris".
    if [ -f /usr/share/zoneinfo/$DOCKER_TIMEZONE ]; then
        echo "$DOCKER_TIMEZONE"
    else
        echo 'Europe/Paris'
    fi
}

_create_symlink(){
    if [ "$1" == '-f' ]; then
        shift
        ln -sf $1 $2
    else
        if [ ! -f $2 ]; then
            ln -s $1 $2
        fi
    fi
}

_echo_gen_rand(){
    tr -dc A-Za-z0-9 < /dev/urandom | head -c 32 ; echo ''
}