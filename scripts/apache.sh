#!/bin/bash

. /scripts/common.sh

# >>>>>>>>>> Activation des configurations

a2enconf zz-common.conf

if [ "$DOCKER_ENVTYPE" = 'dev' ]; then
    a2enconf zz-dev.conf
else
    a2enconf zz-prod.conf
fi

# >>>>>>>>>> Activation du vhost de l'application

if [ -f /app/init/apache2/vhost.conf ]; then
    mv /app/init/apache2/vhost.conf /etc/apache2/sites-available/vhost.conf
    a2ensite vhost.conf
fi

# Suppression des éléments inutiles.
rm -rf /app/init/apache2

# >>>>>>>>>> Démarrage du service

_log "Démarrage du serveur Apache...\n"

apache2ctl -D FOREGROUND

exit $?