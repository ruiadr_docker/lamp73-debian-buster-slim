#!/bin/bash

. /scripts/common.sh

PARAM_UID="$1"
PARAM_GID="$2"
PARAM_USR="$3"
PARAM_GRP="$4"

/usr/sbin/addgroup --gid $PARAM_GID $PARAM_GRP

/usr/sbin/adduser --disabled-password --force-badname \
    --gecos "Utilisateur $PARAM_USR" \
    --gid $PARAM_GID --uid $PARAM_UID \
    --home /home/$PARAM_USR \
    $PARAM_USR

exit 0