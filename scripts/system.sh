#!/bin/bash

. /scripts/common.sh

# >>>>>>>>>> Timezone

_create_symlink -f "/usr/share/zoneinfo/`_echo_ts`" /etc/localtime
_echo_ts > /etc/timezone

# >>>>>>>>>> Utilisateur

# Création de l'utilisateur système associé à la machine hôte.

# APP_UID / APP_GID / APP_USR / APP_GRP doivent impérativement
# exister et être correctement définis.

APP_UID=`echo "$APP_UID" | xargs`
_empty_fatale "$APP_UID" 'APP_UID non défini!'

APP_GID=`echo "$APP_GID" | xargs`
_empty_fatale "$APP_GID" 'APP_GID non défini!'

APP_USR=`echo "$APP_USR" | xargs`
_empty_fatale "$APP_USR" 'APP_USR non défini!'

APP_GRP=`echo "$APP_GRP" | xargs`
_empty_fatale "$APP_GRP" 'APP_GRP non défini!'

/scripts/createuser.sh $APP_UID $APP_GID $APP_USR $APP_GRP

# >>>>>>>>>> Clés SSH

if [ ! -d /home/$APP_USR/.ssh ]; then
    # Création du répertoire s'il n'existe pas encore.

    mkdir /home/$APP_USR/.ssh \
        && chown $APP_USR:$APP_GRP /home/$APP_USR/.ssh \
        && chmod 700 /home/$APP_USR/.ssh

    RSA_INIT='/app/init/ssh/id_rsa'
    RSA_HOME="/home/$APP_USR/.ssh/id_rsa"

    # Clé privée.

    if [ -f $RSA_INIT ]; then
        mv $RSA_INIT $RSA_HOME \
            && chown $APP_USR:$APP_GRP $RSA_HOME \
            && chmod 600 $RSA_HOME
    fi

    # Clé publique.

    if [ -f $RSA_INIT.pub ]; then
        mv $RSA_INIT.pub $RSA_HOME.pub \
            && chown $APP_USR:$APP_GRP $RSA_HOME.pub \
            && chmod 644 $RSA_HOME.pub
    fi
fi

# Suppression des éléments inutiles.
rm -rf /app/init/ssh

# >>>>>>>>>> Utilitaires

_create_symlink /scripts/phpcli.sh /usr/local/bin/phpcli

# >>>>>>>>>> Droits

chown -R $APP_USR:$APP_GRP /app/www

exit 0